//import logo from './logo.svg';
import React, {useEffect, useState} from 'react'
import { Route, Switch, Link} from 'react-router-dom'
import {useParams} from 'react-router'

import './Library/prototypeGroupBy'

import _ from 'lodash'
//import 'semantic-ui-css/semantic.min.css'

//import { Row, Col } from 'react-configurable-grid';
import './App.css';
import SwitchButton from 'react-ios-switch';
import Autocomplete from './Compenents/autocomplete'



// ucus noktalarini json'dan alip, teke düşürme islemi
const origDest = (flight) => {
  let origin = []
  let destination = []
  //console.log(flight)

  flight.forEach(y=>{
  if( ! origin.includes(y.originAirport.name))
      origin.push(y.originAirport.name)
  
  if( !  destination.includes(y.destinationAirport.name ) ) 
      destination.push(y.destinationAirport.name)
  })
 
  let result = {or:origin, ds:destination}
  return result
}

function App() {

  let [collapse, setCollapse] = useState(false)
  let [yolcuSayisi, setYolcuSayisi] = useState(1)
  let [sinif, setSinif] = useState("economy")
  let [ori, setOri] = useState("")
  let [des, setDes] = useState("")

  const [flight, setFlight] = useState([])
  const [destinations, setDestinations] = useState([])
  const [isLoading, setisLoading] = useState(true)
  const [acikAltKategoriler, setAcikAltKategoriler] = useState(0)
  const [stateSwitchButton, setStateSwitchButton] = useState(false)

  useEffect( ()=> {
    fetch("./flights.json")
    .then(response => response.json())
    .then(data => {
      setisLoading ( false )
      setFlight( data.flights )
      localStorage.setItem("flightsStorage", JSON.stringify(data.flights) );

    })


  }, [])  

  const onOrderByEkonomi =()=> {
   // let x = flight.sort( (a,b)=>a.fareCategories.ECONOMY.subcategories[0].price.amount - b.fareCategories.ECONOMY.subcategories[0].price.amount)
   // setFlight(JSON.parse ( localStorage.getItem("flightsStorage")) )
//   console.log(x)

    let x= _.orderBy(flight, 'fareCategories.ECONOMY.subcategories[0].price.amount', 'asc');
    setFlight(x)

  }
  const onOrderByKalkis =()=> {
   // let x = flight.sort( (a,b)=>a.arrivalDateTimeDisplay.subString(0,2) - b.arrivalDateTimeDisplay.subString(0,2))
   setFlight(JSON.parse ( localStorage.getItem("flightsStorage")) )


   //console.log(x)
    // setFlight(x)
  }

  if(isLoading) return <div>Birazcık bekleyiniz...</div>

  const flyTurleri =()=>{}
  
  // origin ve destinationlari json dosyasından alıyoruz.
  let orgDest = origDest(flight)

  const altKategori = (event)=> {
    let x = event.currentTarget.getAttribute("data-id")
    let y = document.getElementById(x)
    

    if( acikAltKategoriler != 0 && acikAltKategoriler != x) {
      document.getElementById(acikAltKategoriler).style.display = "none"
      console.log(acikAltKategoriler +" aa "+ x)
    } 
    y.style.display = "table-row"

    setAcikAltKategoriler(x)
  } 

  document.title = "Türk Hava Yolları - Codechallenge";

  const Header = ()=> {
    return <div id="AnaBaslik">
    <span className="domainname" style={{fontWeight:600}}>
        turkishairlines.com
    </span>
    
    <span className="motto" >  
      <span style={{ fontSize:"12px" }}> search</span> <span style={{fontWeight:600}}>Flight Challenge</span>

    </span>


  </div>
  }

  const Main = () => {
  
    document.body.style.backgroundColor = "#063048";
    document.body.style.color = "#e5ffff";

    return <>

    <div className="main">

      <div>
      <h1>Merhaba</h1>
      <h3 style={{fontWeight:200}}>Nereyi keşfetmek istersiniz?</h3>
      <div className="search-container">
        <div className="search-ic">
          <span className="origin autocomplete"> 
           <i className="icon-origin" style={{marginLeft:"5px"}} > </i> 

           <Autocomplete options={orgDest.or} result={setOri} value={ori}/>
          </span>
          <span className="destination"> 
            <i className="icon-destination" style={{marginLeft:"5px"}} /> 
            <Autocomplete options={orgDest.ds} result={setDes} value={des} />) 
          </span>
          <span style={{marginTop: '2px', width:"70px"}}> 
            <i className="icon-date" /> 
          </span>
          <span style={{position:"relative", width:"35px"}}> 
            <i id="icon-person" onClick={ ()=>setCollapse(!collapse) }>
            </i>
            <span className="yolcu-sayisi2">{yolcuSayisi}</span>

            {collapse ? 
            <div className="collapsing" id="person" style={{position:"absolute", top:"51px", left:"-100px"}}>
              <h4>Kabin ve yolcu seçimi</h4>
              <div id="yolcu-sinifi">
                <form>
                  <input type="radio" id="economy" name="yolcu-sinifi" value="economy" checked={sinif === "economy"} onChange={()=>setSinif("economy")} />
                  <label>Economy Class</label>
                  <input type="radio" id="business" name="yolcu-sinifi" value="business" checked={sinif === "business"} onChange={()=>setSinif("business")} />
                  <label>Business Class</label>
                </form>
              </div>
              <div id="yolcu-sayisi" style={{position: 'relative'}}>
                <span>Yolcu</span>
                <span style={{position: 'absolute', right: '0px'}}>
                  <button id="yolcu-eksi" onClick={()=>yolcuSayisi >1 ? setYolcuSayisi(yolcuSayisi-1) : 1}>-</button>
                  <span id="yolcu-sonuc">{yolcuSayisi}</span>
                  <button id="yolcu-artı" onClick={()=>setYolcuSayisi(yolcuSayisi+1)}>+</button>
                </span>
              </div>
            </div>
            : ""}
          </span>
          <span> 
            <Link to="/ucus" className="icon-go" style={{backgroundColor: '#e81934', paddingTop: '7px'}} /> 
          </span>
        </div>
        </div>
      </div>
    
    </div>
              
    </>
  }

  const KabinSecimi = ({kabin}) => {
    
    let {fiyat} = useParams();



    if(kabin)
    return "Kabin seçiminiz tamamlanamadı"
    
    return <div style={{width:"600px", margin:"auto"}}>

       <h3> Kabin seçiminiz tamamlandı. </h3>
        <div style={{borderTop:"1px solid black", display:"flex", justifyContent:"space-between"}}>
          <h2> Toplam Tutar</h2>
          <h2 style={{fontWeight:500}}>TRY {fiyat}</h2>
        </div>
      </div>
  }

  const Fly = ({subcategories, id}) => {
    return <tr id={id} style={{display:"none"}}>
        <td colSpan={3}>
          {
            subcategories.map((i, k)=>{
              return <>
              
                <div className="ucus-detay">
                    <div className="baslik1">
                      <span>
                        <b>{i.brandCode}</b>
                      </span>
                      <span style={{position:"absolute", right:"5px"}}>
                        <span style={{fontSize:"8px"}}>{i.price.currency}</span> 
                        <b>{stateSwitchButton == true ? i.price.amount / 2 : i.price.amount}</b>
                      </span>
                    </div>

                    <div className="bilgi">
                  {i.rights.map( x=>{
                      return <div className="satir">
                        {x}
                      </div>
                    
                  })}
                  </div>
                    
                    {
                      stateSwitchButton == true ? 
                          k==0 ? 
                            <Link to={"/kabinSecimi/"+i.price.amount/2} className="sec">
                            Uçuşu Seç 
                            </Link>
                          :
                            <Link to="/ucus" className="sec2">
                              Uçuşu Seç 
                            </Link>
                        : 
                        <Link to={"/kabinSecimi/"+i.price.amount} className="sec">
                        Uçuşu Seç 
                        </Link>
                    }
                   
                  </div>
                
                
              </>
            })
          }
          </td>
        </tr>
  }

  const Ucus = () => {
    
    document.body.style.backgroundColor = "white";
    document.body.style.color = "#232b39";

    useEffect (()=> {
      document.getElementById("AnaBaslik").style.borderBottom= "1px solid black";
    }, [])

    return <div className="liste">
        <div className="ucus">
          Uçuş
        </div>
        <div className="guzergah-bilgisi">
          İstanbul - Antalya, 1 Yolcu
        </div>
        <div className="promosyon">
          <span style={{verticalAlign:"super"}}>Promosyon Kodu </span>
            <SwitchButton
              checked={stateSwitchButton}
              onChange={checked => setStateSwitchButton(checked)}
            />
            
            <div className="promosyon-text1">Promosyon kodu seçeneği ile tüm Economy kabini Eco Fly paketlerini %50 indirimle satın alabilirsiniz!</div>
            <div className="promosyon-text2">Promosyon kodu seçeneği aktifken Eco Fly paketi haricinde seçim yapılamamaktadır.</div>
        </div>
        <table>
          <tbody>
          <tr className="baslik">
            <th colSpan={3} style={{padding:"10px"}}>
                <span style={{color:"white", marginRight:"10px", fontSize:"12px"}}>
                Sınama Kriteri
              </span>
              <button onClick={onOrderByEkonomi}>
                Ekonomi Ücreti
              </button>
              <button onClick={onOrderByKalkis}>
                Kalkış Saati
              </button>
            </th>
          </tr>

          {
            flight.map( (i, k)=>{
              

              return <>
                  <tr className="zaman-fiyat">
                <td style={{padding:"10px", width:"290px"}}>
                  <span style={{width:"50px", display:"inline-block"}}>
                    <div style={{fontSize:"14px"}}>{i.arrivalDateTimeDisplay}</div>
                    <div style={{fontSize:"11px"}}>{i.originAirport.city.code}</div>
                    <div style={{fontSize:"10px"}}>{i.originAirport.city.name}</div>
                  </span>
                  <hr style={{display:"inline-block", width:"120px", position:"relative", top:"-7px"}} />
                  <span style={{width:"40px", display:"inline-block", marginLeft:"7px", textAlign:"right"}}>
                    <div style={{fontSize:"14px"}}>{i.departureDateTimeDisplay}</div>
                    <div style={{fontSize:"11px"}}>{i.destinationAirport.city.code}</div>
                    <div style={{fontSize:"10px"}}>{i.destinationAirport.city.name}</div>
                  </span>
                  <span style={{width:"61px", display:"inline-block", marginLeft:"7px", textAlign:"center", position:"relative", top:"-8px"}}>
                    <div style={{fontSize:"8px"}}>Uçuş Süresi</div>
                    <div style={{fontSize:"11px"}}>{i.flightDuration}</div>
                  </span>
                </td>
                <td   style={{cursor:"pointer"}}>
                  <span>
                    <input type="radio" id={"ei"+k} defaultValue="economy2" 
                      name="yolcu-sinifi" 
                      onClick={altKategori}
                      data-id={"e"+k}
                      //value="economy" checked={sinif === "economy"} 
                     // onChange={()=>setSinif("economy")}
                    />
                    <label style={{fontSize:"11px"}}>Economy</label>
                  </span>
                  <span style={{marginLeft:"10px"}}>
                    <div style={{fontSize:"8px"}}>Yolcu Başına</div>
                    <div style={{fontSize:"13px"}}>
                      {i.fareCategories.ECONOMY.subcategories[0].price.currency} 
                    {stateSwitchButton == true ? i.fareCategories.ECONOMY.subcategories[0].price.amount / 2 : i.fareCategories.ECONOMY.subcategories[0].price.amount}
                    </div>
                  </span>
                </td>
                <td   style={{cursor:"pointer"}}>
                  <span>
                    <input type="radio" id={"bi"+k}  defaultValue="business2" 
                        name="yolcu-sinifi" 
                        onClick={altKategori}
                        data-id={"b"+k} 
                        //value="business" checked={sinif === "business"} 
                       // onChange={()=>setSinif("business")}
                    />
                    <label  style={{fontSize:"11px"}}>Business</label>
                  </span>
                  <span style={{marginLeft:"10px"}}>
                    <div style={{fontSize:"8px"}}>Yolcu Başına</div>
                    <div style={{fontSize:"13px"}}>{i.fareCategories.BUSINESS.subcategories[0].price.currency} {i.fareCategories.BUSINESS.subcategories[0].price.amount}</div>
                  </span>
                </td>
              </tr>
              <Fly subcategories={i.fareCategories.ECONOMY.subcategories} id={"e"+k}/>
              <Fly subcategories={i.fareCategories.BUSINESS.subcategories} id={"b"+k}/>
              

              </>
            })
          }
          </tbody>
          
        </table>
        
    </div>
  }

  return <>
    <Switch>
        <Route exact path="/" >
          <Header />
          <Main />
        </Route>

        <Route exact path="/ucus" > 
          <Header />
          <Ucus />
        </Route>

        <Route exact path="/kabinSecimi/:fiyat" > 
          <Header />
          <KabinSecimi />
        </Route>

    </Switch>

    
    </>
  
}

export default App;
